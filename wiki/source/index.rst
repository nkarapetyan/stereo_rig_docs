AFRL  Stereo Rig Sensor Suite
=============================

.. image:: images/DualDPVRig1.png

The AFRL stereo rig is an underwater sensor suite that comprises:

* stereo cameras with modular baseline,
* IMU,
* mechanical scanning profiling sonar,
* depth sensor,
* Intel NUC.

The sensor suite is made with a purpose of localizing and mapping underwater environments. The design objectives of this underwater sensor rig include simplicity of carrying, ease of operation in different modes, and data collection.



.. toctree::
   :hidden:

   Hardware Design <docs/hardware>
   Software Design <docs/software>
   Applications <docs/applications>
   Appendices <docs/appendices>
   License <docs/license>
   Help <docs/help>



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

