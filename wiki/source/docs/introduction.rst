.. _introduction:

===========================
Introduction to AFRL Stereo Rig Sensor Suite
===========================


.. image:: ../images/rig.jpg

The sensor suite is designed to be used by divers for underwater cave exploration expeditions. 
