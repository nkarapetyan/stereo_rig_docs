.. _hardware:

===========================
Hardware Design
===========================


.. image:: ../images/rig.jpg
    :width: 45%
.. image:: ../images/rig_back.jpg
    :width: 40%


To assist vision, the following sensors were employed: Inertial Measurement Unit (IMU), a depth, and an acoustic sensor for accurate state estimation in underwater.
The electronics consists of:
*  two IDS UI-3251LE cameras in a stereo configuration,
*  IMAGENEX 831L Sonar,
*  Microstrain 3DM-GX4-15 IMU,
*  Bluerobotics Bar30 pressure sensor,
*  Intel NUC as the computing unit.

.. figure:: ../images/electronics.jpg
    :width: 50%

    View of electronics of the sensor suite


The two cameras are synchronized via a TinyLily, an Arduino-compatible board, and are capable of capturing images of 1600 x 1200 resolution at 20Hz. The sonar provides range measurement with maximum range of 6m distance, scanning in a plane over 360 degree, with angular resolution of 0.9 degree. A complete scan at 6m takes 4s. Note that the sonar provides for each measurement (point) 255 intensity values, that is 6/255m is the distance between each returned intensity value. Clearly, higher response means a more likely presence of an obstacle. Sediment on the floor, porous material, and multiple reflections result in a multi\hyp modal distribution of intensities. The IMU produces linear accelerations and angular velocities in three axis at a frequency of 100Hz. Finally, the depth sensor produces depth measurements at 1Hz. To enable the easy processing of data, the [ROS framework](www.ros.org) has been utilized for the sensor drivers and for recording timestamped data. 

A 5 inch LED display has been added to provide visual feedback to the diver together with a system based on AR tags is used for changing parameters and to start/stop the recording underwater. The specific hardware of the sensor suite was chosen for compatibility with the Aqua2 Autonomous Underwater Vehicles (AUVs).

The enclosure for the electronics has been designed to ensure ease of operations by divers and waterproofness up to 100m. Two different designs were tested.
