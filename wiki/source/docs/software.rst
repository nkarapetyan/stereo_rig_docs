.. _software:

===========================
Software Design
===========================

The main components of software consist from:

* drivers for each hardware unit
* a ROS interface for communication between sensors and data processing
* an interface for user and rig communication


Drivers
^^^^^^^
The aim of design is to have modular system that ensures re-usability for both the system as whole and also for each component. Each driver provides consistent interface for communication with Robotic Operating System (ROS). The main drivers are:
* UEye driver for each camera - provided by the manufacturer.
* IMU driver that has been developed at Kumar Robotics \cite{imudriver}.
* Sonar driver developed at our -- Autonomous Field Robotics Lab (AFRL).
* Depth - developed at AFRL.


ROS platform 
^^^^^^^^^^^^

For easy data collection we have corresponding nodes for each sensor that publish data. All the operations are performed on NUC that runs linux system. Software was tested both on Ubunut 14.04 and 16.04. When the operating system is loaded the startup script runs all sensor nodes and at the same time starts recording of bag files -- messages -- published on corresponding topics.

Interface
^^^^^^^^^
.. figure:: ../images/menu_use.png

    View of electronics of the sensor suite

The interface consists of two components: menu for online data monitoring and AR tags [FiMa04]_ that supports user and rig communication is adopted from Sattar et al. [SBGD07]_.  Menu supports graphical user interface (GUI) that shows the current video stream of each camera, outputs overall health of system, in addition shows a list of options that user can select. Each operation has a corresponding AR tag associate with its number. Through menus user can troubleshoot stereorig, start or stop recording data, get access to both camera or sonar settings. When camera is selected user can change gain and exposure, perform calibration. In case of entering sonar menu, a new window is opened that visualizes sonar data with rviz.


.. rubric:: References

.. [FiMa04] Fiala, Mark. "Artag revision 1, a fiducial marker system using digital techniques." National Research Council Publication 47419 (2004): 1-47.
.. [SBGD07] Sattar, J., Bourque, E., Giguere, P., & Dudek, G. (2007, May). Fourier tags: Smoothly degradable fiducial markers for use in human-robot interaction. In Computer and Robot Vision, 2007. CRV'07. Fourth Canadian Conference on (pp. 165-174). IEEE.






